package mtp.com.br;
// Eh um teste
import org.junit.Test;
import org.junit.*;

public class OperacoesTest {
	
	Operacoes calc = new Operacoes();
	
	@Test
	public void somar() {		
		Assert.assertEquals(10, calc.somar(5, 5));		
	}
	
	@Test
	public void subtrair() {		
		Assert.assertTrue("M�todo subtrair falhou! Resultado esperado: " + "10" + " resultado atual " 
					+ calc.somar(5,5), calc.somar(5,5) == 10);		
	}
	
	@Test
	public void multiplicar() {
		Assert.assertEquals(30, calc.multiplicar(2, 15));		
	}
	
	@Test
	public void dividir() {
		Assert.assertEquals(10, calc.dividir(100, 10));
	}		
}